# coe332-s2019-hw6
### Preliminary Steps

- Make a private fork of your homework 4 (you can use the hw4 solution if you want)
- Clone your fork to wherever you are running Docker - either your computer or one of the training VMs (especially if you are using Windows)

### Task 1: Build a Docker image and push it to a public Docker hub called coe332-s2019-hw6
You probably have to remove the .git/ directory
`rm -rf .git`

### Task 2: Check that you can pull your public Docker image and run it 
You want to make sure it is working.

For example:
```
docker pull resglowing/coe332-s2019-hw6
docker run -d resglowing/coe332-s2019-hw6
curl http://0.0.0.0:5000/
{"assignments":[{"name":"hw1","points":10,"url":"https://bitbucket.org/jchuahtacc/coe332-f2019-hw1"},{"name":"hw2","points":20,"url":"https://bitbucket.org/jchuahtacc/coe332-f2019-hw2"}],"instructors":[{"email":"jchuah@tacc.utexas.edu","name":"Joon-Yee Chuah"},{"email":"akahn@tacc.utexas.edu","name":"Ari Kahn"},{"email":"charlie@tacc.utexas.edu","name":"Charlie Dey"}],"meeting":{"days":["Tuesday","Thursday"],"end":1330,"location":"GDC 3.248","start":1100}}
```
### Task 3: Slack Jismi a link to your Docker hub repository

### How You Will Be Graded
Jismi will pull and run your coe332-s2019-hw6 Docker image, and then run the curl command to see if it works.

